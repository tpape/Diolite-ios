(function() {
    'use strict';

    angular
        .module('app')
        .directive('dButton', dButton);

    function dButton() {
        return {
            require: ['^dFeature', 'dButton'],
            restrict : 'E',
            scope : true,
            controllerAs : 'vm',
            link : linker,
            bindToController : true,
            controller: controller,
            templateUrl : "app/features/templates/button.html"
        };

        function linker(scope, element, attrs, ctrls) {
            var featureCtrl = ctrls[0];
            var localCtrl = ctrls[1];

            localCtrl.feature = featureCtrl.feature;
            scope.vm = localCtrl;

            localCtrl.activate();
        }
    }

    function controller() {
        var vm = this;
        vm.activate = activate;

        vm.push = push;


        //////////////////////////////////////////////////////////////////////////////////////////////////////////

        function push() {
            vm.feature.send();
        }

        function activate() {
            vm.label = (vm.feature.options.label)?vm.feature.options.label:vm.feature.name;
        }
    }

})();