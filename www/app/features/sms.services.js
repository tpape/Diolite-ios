(function() {
    'use strict';

    angular
        .module('app')
        .factory('smsService', smsService);

    smsService.$inject = ['$localStorage', '$ionicPlatform', '$cordovaSms'];

    /* @ngInject */
    function smsService($localStorage, $ionicPlatform, $cordovaSms) {

        $localStorage.hardware.sms = $localStorage.hardware.sms || {};

        var storage = $localStorage.hardware.sms;

        var service = {
            name : "SMS",
            type : "sms",
            isActuator : true,
            _run : smsRun,
            stop : smsStop,
            setState : smsSetState
        };
        return service;

        ////////////////

        function smsRun() {
            try {
                if (service.state && service.state.id !== 0 && service.state.id !== storage.lastId) {
                    storage.lastId = service.state.id;
                    $ionicPlatform.ready(function() {
                        $cordovaSms.send(service.state.value.recipient, service.state.value.message).then(function() {
                        }, function(error) {
                            console.log("[ERROR] [SMS] : " +  error.code + " : " + error.message);
                        });
                    });
                }
            } catch (error) {
                console.log("Error : ");
                console.log(error);
            }
        };

        function smsStop() {
        };

        function smsSetState(state){
            service.state = state;
            service.run();
        };
    }
})();