(function() {
    'use strict';

    angular
    .module('app')
    .factory('featureService', featureService);

    featureService.$inject = ['$q', '$FEATURE_URL_PATH', 'accelerometerService', 'gpsService', 'buzzerService', 'smsService', 'nfcService', 'connectionService', 'errorService'];
    function featureService($q, $FEATURE_URL_PATH, accelerometerService, gpsService, buzzerService, smsService, nfcService, connectionService, errorService){

        var boards = {};
        var hardwareFeatures = {};
        var types = {
            'button' : {},
            'switch' : {},
            'slider' : {},
            'msg-sender' : {},
            'gauge' : {
                isActuator : true
            },
            'msg-receiver' : {
                isActuator : true
            },
            'light' : {
                isActuator : true
            }
        };

        retrieveHardwareFeatures();

        return {
            'types' : types,
            'getBoards' : getBoards,
            'getBoard' : getBoard,
            'newFeature' : newFeature,
            'deleteBoard' : deleteBoard,
            'deleteFeature' : deleteFeature
        };

        ////////////////////////////////////////////////////////////////////////////////////////////////////////////

        function getBoards() {
            // return the promise with a registered callback wich the return value will be injected if another callback is defined
            // (see chained promises)
            return connectionService.get($FEATURE_URL_PATH).then(callBack, callBackError);

            function callBack(response) {

                boards = {};
                connectionService.purge();
                retrieveHardwareFeatures();

                response.data.forEach(function(feature){
                    formatFeature(feature);
                    storeFeature(feature);
                });

                return boards;
            }

            function callBackError(error) {
                return errorService.report("Unable to retrieve objects", error).then(function(){
                    return getBoards();
                }, function(error) {
                    console.log(error);
                });
            }
        }

        function getBoard(board) {
            return boards[board];
        }

        function deleteBoard(board) {
            boards[board].forEach(function(feature) {
                deleteFeature(feature);
            });

            delete boards[board];
        }

        function deleteFeature(feature) {
            var boardId = feature.board;
            var board = boards[boardId];

            if (board && !feature.isHardware) {
                boards[boardId].splice(board.indexOf(feature), 1);
            }

            return connectionService.delete(feature).then(function(){
                return board;
            });
        }

        function newFeature(feature) {

            var prefix = (feature.global)?"global/":(connectionService.config.deviceName + "/");
            var board  = (feature.isHardware)?"hardware/":(feature.board + "/");
            var name = feature.name;

            feature.id = prefix + board + name;
            feature.isActuator = types[feature.type].isActuator;

            var tmpFeature = {
                id : feature.id,
                type : feature.type,
                options : feature.options
            };

            return connectionService.post($FEATURE_URL_PATH, tmpFeature).then(callBack);

            function callBack() {
                formatFeature(feature);
                storeFeature(feature);
            }
        }

        ////////////////////////////////////////////////////////////////////////////////////////////////////

        function formatFeature(feature) {
            var idSplitted = feature.id.split('/');

            feature.name =  idSplitted[2];
            feature.board = idSplitted[1];
            feature.device =  idSplitted[0];

            feature.isActuator = types[feature.type].isActuator;
            feature.isHardware = feature.board === "hardware";

            feature.state = feature.state || {
                id : 0
            }

            if (feature.isActuator) {
                feature.refresh = function() {
                    return connectionService.get($FEATURE_URL_PATH + "/" + feature.id);
                };

                feature.setState = types[feature.type].setState || function(state) {
                    feature.state = state;
                };
            } else {
                feature.send = function(data) {
                    return connectionService.send(feature.id, {
                        'data' : data
                    });
                };
            }
        }

        function storeFeature(feature) {
            if (feature.device !== "global" && feature.device !== connectionService.config.deviceName) {
                return;
            }

            if (!feature.isHardware) {
                boards[feature.board] = boards[feature.board] || [];
                boards[feature.board].push(feature);
            } else {
                angular.merge(hardwareFeatures[feature.name], feature);
                hardwareFeatures[feature.name].run();
                hardwareFeatures[feature.name].activate = true;
            }

            if (feature.isActuator) {
                connectionService.register(feature);
            }
        }

        function retrieveHardwareFeatures() {
            boards.hardware = [];
            boards.hardware.push(formatHardwareFeature(accelerometerService));
            boards.hardware.push(formatHardwareFeature(gpsService));
            boards.hardware.push(formatHardwareFeature(buzzerService));
            boards.hardware.push(formatHardwareFeature(smsService));
            boards.hardware.push(formatHardwareFeature(nfcService));
        }

        function formatHardwareFeature(feature) {
            feature.isHardware = true;
            feature.activate = false;
            feature.run = run;
            feature.init = init;
            feature.destroy = destroy;
            feature.change = change;
            types[feature.type] = types[feature.type] || feature;
            hardwareFeatures[feature.name] = feature;

            function run(){
                try {
                    feature._run();
                } catch(error) {
                    console.log("Error on running hardware feature type : " + feature.type);
                    console.log(error);
                    feature.activate = false;
                }
            }

            function init(){
                newFeature(feature);
            };

            function destroy() {
                try {
                    feature.stop();
                } finally {
                    deleteFeature(feature);
                }
            };

            function change(){
                if (feature.activate) {
                    feature.init();
                } else {
                    feature.destroy();
                }
            };

            return feature;
        }
    };

})();