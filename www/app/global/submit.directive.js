(function() {
    'use strict';

    angular
        .module('app')
        .directive('touchSubmit', touchSubmit);

    touchSubmit.$inject = ['$compile'];

    /*********************************************

    Generate an invisible submit input to enable the "return" key standard behaviour by copying the on-tap action
    Have to be used inside of a form.

    *********************************************/

    /* @ngInject */
    function touchSubmit($compile) {
        var directive = {
            link: link,
            restrict: 'A',
        };
        return directive;

        function link(scope, element, attrs, controller) {
        	var template = "<button type='submit' style='display:none' data-ng-click='" + attrs.onTap + "' ></button>";
        	element.after($compile(template)(scope));
        }
    }
})();