/********
    author : Thomas Pape (http://thomas-pape.fr)
*********/


(function() {
    'use strict';

    angular.module('app', ['ionic', 'ngCordova', 'ngStorage'])

    .constant("$MODEL_VERSION", "1.0.0")
    .constant("$VERSION", "1.1.1")
    .constant("$REFRESH_HTTPREST_TIMEOUT", 1)
    .constant("$DEFAULT_PORT", "8080")
    .constant("$DEFAULT_PROTOCOL", "http")
    .constant("$FEATURE_URL_PATH", "/feature")
    .constant("$LOGIN_URL_PATH", "/login")

    .config(config)

    .run(run);

    ///////////////////////////////////////////////////////////////////////////

    config.$inject = ['$ionicConfigProvider', '$localStorageProvider', '$MODEL_VERSION'];
    function config($ionicConfigProvider, $localStorageProvider, $MODEL_VERSION) {
        // requiered for Android
        $ionicConfigProvider.navBar.alignTitle('center');

        if ($localStorageProvider.get('version') !== $MODEL_VERSION) {
            $localStorageProvider.set('version', $MODEL_VERSION);
            $localStorageProvider.set('hardware', {});
            $localStorageProvider.set('login', null);
            $localStorageProvider.set('config', null);
        }
    };

    run.$inject = ['$ionicPlatform'];
    function run($ionicPlatform) {
        $ionicPlatform.ready(function() {

            window.shouldRotateToOrientation = function(degrees) {
                return true;
            }

            // Hide the accessory bar by default (remove this to show the accessory bar above the keyboard
            // for form inputs)
            if(window.cordova && window.cordova.plugins.Keyboard) {
                cordova.plugins.Keyboard.hideKeyboardAccessoryBar(true);
            }

            if(window.StatusBar) {
                StatusBar.styleDefault();
            }
        });
    };
})();
