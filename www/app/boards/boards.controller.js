(function() {
    'use strict';

    angular.module('app')

    .controller('BoardsController', BoardsController);

    BoardsController.$inject = ['$rootScope', '$scope', '$state', '$cordovaDeviceMotion', 'featureService'];
    function BoardsController($rootScope, $scope, $state, $cordovaDeviceMotion, featureService) {
        var vm = this;

        vm.boards = {};
        vm.getBoardsLength = getBoardsLength;

        vm.doRefresh = doRefresh;
        vm.addBoard = addBoard;
        vm.deleteBoard = deleteBoard;
        vm.loadBoard = function(name) { $state.go('features', {boardName : name}); };

        $rootScope.$on('loggedIn', function(){
            activate();
        });

        activate();

        ////////////////////////////////////////////////////////////////////////////////////////////////////

        function addBoard(valid) {
            if (valid && vm.newBoardName && vm.newBoardName !== "" && !vm.boards[vm.newBoardName]) {
                vm.boards[vm.newBoardName] = [];
                vm.newBoardName = "";
            }
        };

        function deleteBoard(id) {
            featureService.deleteBoard(id);

            if (getBoardsLength() === 0) {
                vm.displayDelete(false);
            }
        }

        function getBoardsLength() {
            if (vm.boards) {
                return Object.keys(vm.boards).length;
            } else {
                return 0;
            }
        }

        function activate() {

            return featureService.getBoards().then(callBackOk, callBackError);

            function callBackOk(data) {
                vm.boards = data;
            };

            function callBackError(error) {
                console.log(error);
            };
        };

        function doRefresh() {
            activate().then(function(){
                $scope.$broadcast('scroll.refreshComplete');
            });
        };

    };
})();