## iOS client application for the BeC3 IoT solution  

More informations : [bec3.com](http://bec3.com).

The client is based on this frameworks :   
- Apache Cordova, Apache License v2.0, [cordova.apache.org/](http://cordova.apache.org/)
- Ionic, MIT License, [ionicframework.com](http://ionicframework.com/)
- AngularJS, MIT License, [angularjs.org](https://www.angularjs.org/)


Feel free to report any issue.  
Feel free to contribute.